import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { ConnectableObservable } from 'rxjs';
import { User } from './User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'Orange Angular App';

  user: User | undefined = undefined

  login() {
    // debugger
    this.user = {
      name: 'Alice'
    }
    // rerendeer
    // this.cdr.detectChanges()
  }

  check() {
    console.log('check');

  }

  constructor(private cdr: ChangeDetectorRef) {
    // cdr.detach()

    // (this.user as User).name
    if (this.user == undefined) {
      // this.login()
    }
  }

  // ngOnInit(){
  //   this.cdr.detectChanges()
  // }
}
