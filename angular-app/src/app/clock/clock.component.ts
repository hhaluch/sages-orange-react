import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, NgZone, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockComponent implements OnInit {

  @Input() settings = ''

  time = (new Date()).toLocaleTimeString()

  constructor(private cdr: ChangeDetectorRef,
    private ngZOne: NgZone) {
    // cdr.detach()
  }

  ngOnInit(): void {
    this.ngZOne.runOutsideAngular(() => {
      setInterval(() => {

        this.time = (new Date()).toLocaleTimeString()
        this.cdr.detectChanges()
      }, 1000)
    })
  }

}
