import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from '../User';

@Component({
  selector: 'app-testform',
  templateUrl: './testform.component.html',
  styleUrls: ['./testform.component.css']
})
export class TestformComponent implements OnInit {

  @Input('user') user!: User

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
  }

  submit(data: any) {
    this.http.post('/', data).subscribe()
  }

}
