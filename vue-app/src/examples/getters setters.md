```ts

obj = {
    _secret:'',
    get value(){ 
        return this._secret.split('').reverse().join('')
    },
    set value(v){
        this._secret = v.split('').reverse().join('')
    },
}
{_secret: ''}value: (...)_secret: ""get value: ƒ value()set value: ƒ value(v)[[Prototype]]: Object
obj.value = 'Ala ma kota'
'Ala ma kota'
obj
{_secret: 'atok am alA'}
obj.value 
'Ala ma kota'

```