## GIT

https://bitbucket.org/ev45ive/sages-orange-react/commits/

git clone https://bitbucket.org/ev45ive/sages-orange-react/ sages-orange-react
cd sages-orange-react
npm i 
npm run start
npm run storybook

## Snippets

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

## Devtools
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi
## Playlist module

mkdir -p src/playlists/containers/
mkdir -p src/playlists/components/

mkdir -p src/core/components/
mkdir -p src/core/model

touch src/playlists/containers/PlaylistView.tsx

touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditor.tsx

touch src/core/components/Button.tsx

# Search module

mkdir -p src/music/containers/
mkdir -p src/music/components/

mkdir -p src/core/services
touch src/core/services/SpotifyAPIService.tsx

touch src/music/containers/AlbumSearchView.tsx

touch src/music/components/SearchForm.tsx
touch src/music/components/ResultsGrid.tsx
touch src/music/components/AlbumCard.tsx

## Generate types
https://quicktype.io

## HTTP hooks libraries:
https://use-http.com/#/
https://react-query.tanstack.com/
https://swr.vercel.app/

https://github.com/axios/axios

## Oauth2.0
https://developer.spotify.com/documentation/general/guides/authorization/implicit-grant/
npm i react-oauth2-hook immutable react-storage-hook  prop-types

## React Query + Axios
npm i react-query axios

## Mock Service Worker
https://storybook.js.org/addons/msw-storybook-addon
https://mswjs.io/
https://github.com/storybookjs/storybook/blob/next/addons/interactions/README.md

## Server configuration for HistoryAPI Push URL ( BrowserRouter )
https://angular.io/guide/deployment#server-configuration
