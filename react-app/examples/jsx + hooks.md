```tsx

window.React = React;
window.ReactDOM = ReactDOM;

interface User {
  id: string;
  name: string;
  color: string;
  pet: {
    name: string;
  };
}

const users: User[] = [
  { id: "1", name: "Alice", pet: { name: "Cat" }, color: "red" },
  { id: "2", name: "Bob", pet: { name: "Dog" }, color: "green" },
  { id: "3", name: "Kate", pet: { name: "Fish" }, color: "blue" },
];

// React: currentNode = ...
const PersonItem = (props: {
  user: User;
  counter: number;
  onCounterChange: (counter: any) => void;
}) => {
  // let counter = 1; // will be reset to "1" each rerender

  // const counterState = useState(1);
  // const counter = counterState[0];
  // const setCounter = counterState[1];
  // const [counter, setCounter] = useState(1);
  const [active, setActive] = useState(false);

  return (
    <li id={props.user.id} className="list-group-item">
      <p className="display-5" style={{ color: props.user.color }}>
        {props.user.name} has a {props.user.pet.name} [{props.counter}]
        <button
          onClick={() => {
            // setCounter(counter + 1);
            props.onCounterChange(props.counter + 1);
          }}
        >
          +
        </button>
        <button onClick={() => setActive(!active)}>
          {active ? "Disable" : "Activate"}
        </button>
      </p>
      {/* <input type="text" /> */}
    </li>
  );
};

const Lista = () => {
  const [counters, setCounters] = useState([1, 2, 3]);
  return (
    <ul className="list-group">
      <li>{JSON.stringify(counters)}</li>
      {/* === */}
      {users.map((user, index) => (
        <PersonItem
          counter={counters[index]}
          user={user}
          key={user.id}
          onCounterChange={(counter) => {
            setCounters((counters) => {
              counters[index] = counter;
              return [...counters]; // Has to be copy if mutated
            });
          }}
        />
      ))}
    </ul>
  );
};
ReactDOM.render(<Lista />, document.getElementById("root"));
```