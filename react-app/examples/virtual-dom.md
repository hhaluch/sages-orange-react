
```tsx

 const PersonItem = (props: { user: User }) => {
   return React.createElement(
     "li",
     { id: "123", className: "list-group-item" },
     React.createElement(
       "p",
       { className: "display-5", style: { color: props.user.color } },
       `${props.user.name} has a  ${props.user.pet.name}`
     )
   );
 };

 export class PersonForm extends React.Component {
   state = { counter: 1 };
   componentDidMount() {
     setInterval(() => {
       this.setState((state: any) => ({ counter: state.counter + 1 }));
     }, 1000);
   }
   // componentWillUnmount(){ clearInterval... }
   render() {
     return (
       <form>
         <input type="text" /> <button>Submit</button>
         {this.state.counter}
       </form>
     );
   }
 }

 const vlist = React.createElement(
   "ul",
   { className: "list-group" },
   users.map((user) => {
      return PersonItem({ user: user });
     return React.createElement(PersonItem, { user: user });
   }),
   React.createElement(PersonForm)
 );

```