```tsx

interface Point {
  x: number;
  y: number;
}

interface Vector {
  x: number;
  y: number;
  length:number
}

let p: Point = { x: 123, y: 123 };
let v: Vector = { x: 123, y: 123, length:234 };

p = v;
// v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.
// p.length // Property 'length' does not exist on type 'Point'.
(p as Vector).length

```