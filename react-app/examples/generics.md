```tsx

const arr1: number[] = [1, 2, 3];
const arr2: (string | number)[] = ["A", "B", "C"];
const arr3: Date[] = [new Date(), new Date()];

arr2.push("A", 123);

function getFirstItem<T>(arr: T[]): T {
  return arr[0];
}

// const r1 = getFirstItem(arr1);
// r1.toLocalTime();

const a = getFirstItem<number>([1, 2, 3]); // function getFirstItem<number>(arr: number[]): number
const b = getFirstItem(["a", "b", "c"]); // function getFirstItem<string>(arr: string[]): string
const c: Date = getFirstItem([new Date(), new Date()]); // function getFirstItem<string>(arr: string[]): string

class Queue<T> {
  constructor(private items: T[] = []) {
    // this.items = items;
  }
  push(item: T) {
    this.items.push(item);
  }
  pop(): T | undefined {
    return this.items.pop();
  }
}
const q = new Queue(['123']);
// q.items // Property 'items' is private and only accessible within class 'Queue<T>'.ts(2341)
// q.push(123);
q.push("123");

```