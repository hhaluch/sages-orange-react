import { useLayoutEffect, useMemo, useRef } from "react";
import { useOAuth2Token } from "react-oauth2-hook";
import axios from "axios";
import { isSpotifyErrorResponse } from "./core/model/Album";

export function useAuth() {
  const [token, getToken, setToken] = useOAuth2Token({
    authorizeUrl: "https://accounts.spotify.com/authorize",
    scope: [
      "user-library-read",
      "playlist-modify-private",
      "playlist-read-collaborative",
      "playlist-read-private",
      "playlist-modify-public",
    ],
    clientID: "ca8145259c0a468589cb3842f23014a0",
    redirectUri: document.location.origin + "/callback",
  });

  // useLayoutEffect(() => {
  //   if (!token) getToken();
  // }, []);

  useMemo(() => {
    axios.interceptors.response.use(
      (resp) => resp,
      (err) => {
        // debugger
        if (!axios.isAxiosError(err)) {
          throw new Error("Unexpected server error");
        }
        if (err.response?.status == 401) {
          // setTimeout(() => {
          //   setToken(undefined);
          // }, 1000);
          throw new Error("Not logged in");
        }

        if (err.response && isSpotifyErrorResponse(err.response.data)) {
          throw new Error(err.response.data.error.message);
        }
        throw new Error("Unexpected error");
      }
    );
  }, []);

  const requestInterceptorHandler = useRef<number>(null);
  useMemo(() => {
    axios.defaults.baseURL = "https://api.spotify.com/v1/";
    // console.log(requestInterceptorHandler.current);

    if (requestInterceptorHandler.current) {
      axios.interceptors.request.eject(requestInterceptorHandler.current);
    }
    (requestInterceptorHandler.current as any) = axios.interceptors.request.use(
      (config) => {
        config.headers = {
          ...config.headers,
          Authorization: "Bearer " + token,
        };
        return config;
      }
    );
  }, [token]);

  return { token, getToken, setToken };
}
