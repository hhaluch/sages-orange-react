import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { PlaylistDetails } from "./PlaylistDetails";
import { playlistsData } from "../containers/playlistsData";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistDetails",
  component: PlaylistDetails,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof PlaylistDetails>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistDetails> = (args) => (
  <PlaylistDetails {...args} />
);

export const NoPlaylist = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
NoPlaylist.args = {};

export const PlaylistSelected = Template.bind({});
PlaylistSelected.args = {
  playlist: playlistsData[1],
};

export const PlaylistPublicSelected = Template.bind({});
PlaylistPublicSelected.args = {
  playlist: playlistsData[0],
};
