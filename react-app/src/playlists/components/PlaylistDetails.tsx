// tsrafc
import React from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  /**
   * Playlist data
   */
  playlist?: Playlist;
  onEdit: (playlist_id: Playlist["id"]) => void;
  onCancel?: () => void;
}

export const PlaylistDetails = ({ playlist, onEdit, onCancel }: Props) => {
  if (!playlist)
    return <p className="alert alert-info">No playlist selected</p>;

  return (
    <div>
      {/* dl>(dt{Name:}+dd{test})*3 */}
      <dl>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>

        <dt>Public:</dt>
        {/* {isPublic} */}

        {playlist.public ? <dd style={{ color: "green" }}>Yes</dd> : null}
        {!playlist.public && <dd style={{ color: "red" }}>No</dd>}

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <button
          type="button"
          className="btn btn-info"
          onClick={() => onEdit(playlist.id)}
        >
          Edit
        </button>
        {onCancel && (
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => onCancel()}
          >
            Cancel
          </button>
        )}
      </div>
    </div>
  );
};
