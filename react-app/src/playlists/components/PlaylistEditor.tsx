import { v4 as uuid } from "uuid";
import React, { useEffect, useState } from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist?: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => void;
}

const EMPTY_PLAYLIST = {
  id: "",
  name: "",
  public: false,
  description: "",
};

export const PlaylistEditor = ({
  playlist = EMPTY_PLAYLIST,
  onCancel,
  onSave,
}: Props) => {
  const [playlistId, setPlaylistId] = useState(() => playlist.id || uuid());
  const [playlistName, setPlaylistName] = useState(playlist.name);
  const [playlistPublic, setPlaylistPublic] = useState(playlist.public);
  const [playlistDescription, setPlaylistDescription] = useState(
    playlist.description
  );

  useEffect(() => {
    setPlaylistId(playlist.id);
    setPlaylistName(playlist.name);
    setPlaylistPublic(playlist.public);
    setPlaylistDescription(playlist.description);
  }, [playlist]);

  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    onSave({
      id: playlistId,
      name: playlistName,
      public: playlistPublic,
      description: playlistDescription,
    });
  };

  return (
    <form onSubmit={submit}>
      {/* <pre> {JSON.stringify(playlist, null, 2)}</pre>
      <pre>
        {JSON.stringify(
          { playlistId, playlistName, playlistPublic, playlistDescription },
          null,
          2
        )}
      </pre> */}

      <div className="form-group mb-3">
        <label htmlFor="playlist_name">Name</label>
        <input
          type="text"
          className="form-control"
          name="playlist_name"
          id="playlist_name"
          placeholder="Name"
          value={playlistName}
          onChange={(event) => setPlaylistName(event.target.value)}
        />
        <small id="helpId" className="form-text text-muted float-end">
          {playlistName.length} / 100
        </small>
      </div>

      <div className="form-check mb-3">
        <label className="form-check-label">
          <input
            type="checkbox"
            className="form-check-input"
            name="playlist_public"
            id="playlist_public"
            checked={playlistPublic}
            onChange={(e) => setPlaylistPublic(e.target.checked)}
          />
          Public
        </label>
      </div>

      <div className="form-group mb-3">
        <label htmlFor="playlist_description">Description</label>
        <textarea
          className="form-control"
          name="playlist_description"
          id="playlist_description"
          rows={3}
          value={playlistDescription}
          onChange={(event) => setPlaylistDescription(event.target.value)}
        ></textarea>
      </div>

      <button type="button" className="btn btn-danger" onClick={onCancel}>
        Cancel
      </button>
      <button type="submit" className="btn btn-success">
        Save
      </button>
    </form>
  );
};
