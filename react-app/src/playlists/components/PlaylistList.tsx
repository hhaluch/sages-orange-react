import React, { useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "../../core/helpers";

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
  onSelectedChange: (id: string) => void;
  onDelete: (id: string) => void;
}

export const PlaylistList = ({
  playlists,
  selectedId, // Input
  onSelectedChange, //  Output
  onDelete,
}: Props) => {
  return (
    <div>
      <ul className="list-group">
        {playlists.map((playlist, index) => {
          return (
            <li
              className={cls(
                "list-group-item",
                selectedId === playlist.id && "active"
              )}
              key={playlist.id}
              onClick={() => onSelectedChange(playlist.id)}
            >
              {index + 1}. {playlist.name}
              <span
                className="close float-end"
                onClick={(event) => {
                  event.stopPropagation();
                  onDelete(playlist.id);
                }}
              >
                &times;
              </span>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
