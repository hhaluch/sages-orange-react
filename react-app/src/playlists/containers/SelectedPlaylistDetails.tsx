import React from "react";
import { useQuery } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import { Loading } from "../../core/components/Loading";
import { fetchPlaylistById } from "../../core/services/SpotifyAPIService";
import { PlaylistDetails } from "../components/PlaylistDetails";

type Props = {};

export const SelectedPlaylistDetails = (props: Props) => {
  const { playlist_id } = useParams();
  const navigate = useNavigate();

  const { data, isLoading, error } = useQuery(["playlists", playlist_id], () =>
    fetchPlaylistById(playlist_id)
  );

  if (!data) return <Loading />;

  return (
    <div>
      <PlaylistDetails
        playlist={data}
        onEdit={() => navigate("edit")}
        onCancel={() => navigate("/playlists")}
      />
    </div>
  );
};
