import React, { useEffect, useState } from "react";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistEditor } from "../components/PlaylistEditor";
import { PlaylistList } from "../components/PlaylistList";
import { Playlist } from "../../core/model/Playlist";
import { playlistsData } from "./playlistsData";
import { useFetchSearch } from "../../core/services/useFetchSearch";
import { fetchPlaylists } from "../../core/services/SpotifyAPIService";

interface Props {}

export const PlaylistView = ({}: Props) => {
  const [mode, setMode] = useState<"details" | "edit" | "create">("details");
  const [playlists, setPlaylists] = useState<Playlist[]>([]);

  const [selectedId, setSelectedId] = useState<Playlist["id"] | undefined>();
  const [selected, setSelected] = useState<Playlist | undefined>();

  // const { results: playlists } = useFetchSearch([], () => {
  //   return fetch("playlists.json").then((resp) => resp.json());
  // });

  useEffect(() => {
    fetchPlaylists().then((data) => setPlaylists(data));
  }, []);

  useEffect(() => {
    setSelected(playlists.find((p) => p.id === selectedId));
  }, [playlists, selectedId]);

  const selectPlaylistById = (id: string) => {
    setSelectedId(id);
  };

  const editMode = () => {
    setMode("edit");
  };
  const createMode = () => {
    setMode("create");
  };
  const cancel = () => {
    setMode("details");
  };

  const deletePlaylist = (id: Playlist["id"]) => {
    setMode("details");
    setPlaylists(playlists.filter((p) => p.id !== id));
    setSelectedId(selectedId === id ? undefined : id);
  };

  const savePlaylist = (draft: Playlist) => {
    setMode("details");
    setPlaylists(playlists.map((p) => (p.id === draft.id ? draft : p)));
    setSelectedId(draft.id);
  };
  const createPlaylist = (draft: Playlist) => {
    setMode("details");
    setPlaylists([...playlists, draft]);
    setSelectedId(draft.id);
  };

  return (
    <div>
      <div className="display-5 mb-2">Playlists</div>
      {/*  */}
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selectedId={selectedId}
            onSelectedChange={selectPlaylistById}
            onDelete={deletePlaylist}
          />

          <button className="btn btn-info float-end mt-5" onClick={createMode}>
            Create New
          </button>
        </div>
        <div className="col">
          {selected && mode === "details" && (
            <PlaylistDetails playlist={selected} onEdit={editMode} />
          )}

          {selected && mode === "edit" && (
            <PlaylistEditor
              playlist={selected}
              onCancel={cancel}
              onSave={savePlaylist}
            />
          )}

          {selected && mode === "create" && (
            <PlaylistEditor onCancel={cancel} onSave={createPlaylist} />
          )}

          {!selected && mode !== "create" && (
            <p className="alert alert-info">Please select playlist</p>
          )}
        </div>
      </div>
    </div>
  );
};

// class C extends React.Component{
//   constructor(){}
//   componentDidMount(){}
//   componentDidUpdate(){  if(this.state.x === nextState.x){...} }
//   componentWillUnmount(){}
// }
