import React from "react";
import { useQuery } from "react-query";
import { Outlet, useNavigate, useParams } from "react-router-dom";
import { Loading } from "../../core/components/Loading";
import { fetchPlaylists } from "../../core/services/SpotifyAPIService";
import { PlaylistList } from "../components/PlaylistList";
import { playlistsData } from "./playlistsData";

type Props = {};

export const PlaylistsContainerView = (props: Props) => {
  const { playlist_id } = useParams();
  const navigate = useNavigate();

  const {
    data: playlists,
    isLoading,
    error,
  } = useQuery(["playlists"], () => fetchPlaylists());

  const selectPlaylistById = (id: string): void => {
    navigate(`/playlists/${id}`);
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <h3 className="display-4">Playlists</h3>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists || []}
            onSelectedChange={selectPlaylistById}
            onDelete={() => {}}
            selectedId={playlist_id}
          />
        </div>
        <div className="col">
          <Outlet />
        </div>
      </div>
    </div>
  );
};
