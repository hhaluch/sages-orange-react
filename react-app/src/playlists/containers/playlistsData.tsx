import { Playlist } from "../../core/model/Playlist";

export const playlistsData: Playlist[] = [
  {
    id: "123",
    name: "Playlist ABC",
    public: true,
    description: "Opis 123",
  },
  {
    id: "234",
    name: "Playlist BCD",
    public: false,
    description: "Opis 234",
  },
  {
    id: "345",
    name: "Playlist CDE",
    public: true,
    description: "Opis 345",
  },
];
