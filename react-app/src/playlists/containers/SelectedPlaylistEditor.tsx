import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import { Loading } from "../../core/components/Loading";
import { Playlist } from "../../core/model/Playlist";
import {
  fetchPlaylistById,
  updatePlaylist,
} from "../../core/services/SpotifyAPIService";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistEditor } from "../components/PlaylistEditor";

type Props = {};

export const SelectedPlaylistEditor = (props: Props) => {
  const { playlist_id } = useParams();
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const playlistMutation = useMutation((draft: Playlist) =>
    updatePlaylist(draft)
  );

  const { data, isLoading, error } = useQuery(["playlists", playlist_id], () =>
    fetchPlaylistById(playlist_id)
  );

  if (!data) return <Loading />;

  const saveChanges = async (draft: Playlist) => {
    await playlistMutation.mutateAsync(draft);
    await queryClient.invalidateQueries(["playlists"], { active: true });
    setTimeout(() => navigate(".."), 1000);
  };
  return (
    <div>
      {playlistMutation.isLoading && <p>Saving changes</p>}
      {playlistMutation.isSuccess && <p>Saved changes</p>}

      <PlaylistEditor
        playlist={data}
        onSave={saveChanges}
        onCancel={() => navigate("..")}
      />
    </div>
  );
};
