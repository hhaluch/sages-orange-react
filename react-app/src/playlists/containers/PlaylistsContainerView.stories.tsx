import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { rest } from "msw";
import { PlaylistsContainerView } from "./PlaylistsContainerView";
import { playlistsData } from "./playlistsData";
import { Routes, Route, Navigate } from "react-router-dom";
import { SelectedPlaylistDetails } from "./SelectedPlaylistDetails";
import { SelectedPlaylistEditor } from "./SelectedPlaylistEditor";
import { Playlist } from "../../core/model/Playlist";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistsView",
  component: PlaylistsContainerView,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
  parameters: {
    msw: {
      handlers: [
        rest.get("me/playlists", (req, res, ctx) => {
          return res(ctx.delay(), ctx.json({ items: playlistsData }));
        }),
        rest.get("playlists/:id", (req, res, ctx) => {
          return res(
            ctx.delay(),
            ctx.json(playlistsData.find((p) => p.id === req.params.id))
          );
        }),
        rest.put<Playlist>("playlists/:id", (req, res, ctx) => {
          const index = playlistsData.findIndex((p) => p.id === req.params.id);
          playlistsData[index] = {
            ...playlistsData[index],
            name: req.body?.name,
          };
          return res(ctx.delay(), ctx.json({}));
        }),
      ],
    },
  },
} as ComponentMeta<typeof PlaylistsContainerView>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistsContainerView> = (args) => (
  <>
    <Routes>
      <Route path="*" element={<Navigate to="/playlists/" />} />
      <Route path="/playlists/" element={<PlaylistsContainerView />}>
        <Route index element={<p>Please select playlist</p>} />
      </Route>

      <Route
        path="/playlists/:playlist_id"
        element={<PlaylistsContainerView />}
      >
        <Route index element={<SelectedPlaylistDetails />} />
        <Route path="edit" element={<SelectedPlaylistEditor />} />
      </Route>
    </Routes>
  </>
);

export const Primary = Template.bind({});
Primary.args = {};
