// tsrafc
import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import { Album } from "../../core/model/Album";
import { fetchAlbumSearchResults } from "../../core/services/SpotifyAPIService";
import { useFetchSearch } from "../../core/services/useFetchSearch";
import { ResultsGrid } from "../components/ResultsGrid";
import { SearchForm } from "../components/SearchForm";

type Props = {};

export const AlbumSearchView = (props: Props) => {
  // const location = useLocation();
  const navigate = useNavigate();
  let [searchParams, setSearchParams] = useSearchParams({ q: "" });

  const currentQuery = searchParams.get("q") || "";

  const { isLoading, error, data } = useQuery(
    ["albumSearch", currentQuery],
    () => (currentQuery ? fetchAlbumSearchResults(currentQuery) : undefined)
  );

  const search = (query: string) => {
    setSearchParams({ q: query }, { replace: true });
    // navigate({ pathname: "/search", search: "?q=" + query }, { replace: true });
  };

  return (
    <div>
      <h3 className="display-3">Albums Search</h3>
      <div className="row">
        <div className="col">
          <SearchForm value={currentQuery} onSearch={search} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {error instanceof Error && (
            <p className="alert alert-danger">{error.message}</p>
          )}
          {isLoading === true && <p>Loading...</p>}

          {!isLoading && data && (
            <>
              <p>
                Found {data.length} for query "{currentQuery}"
              </p>
              <ResultsGrid results={data} />
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default AlbumSearchView