import React from "react";
import { useQuery } from "react-query";
import { useLocation, useParams, useSearchParams } from "react-router-dom";
import { Loading } from "../../core/components/Loading";
import { Album, Artist } from "../../core/model/Album";
import { fetchAlbumById } from "../../core/services/SpotifyAPIService";
import { AlbumCard } from "../components/AlbumCard";
import { TracksList } from "../components/TracksList";

type Props = {};

export const AlbumDetailView = (props: Props) => {
  // /albums/?id=XXXX
  // const [params] = useSearchParams();
  // const id = params.get("id");
  
  // /albums/XXXX
  const { album_id } = useParams();

  const {
    isLoading,
    data: album,
    error,
    refetch,
  } = useQuery(["albums", album_id], () => fetchAlbumById(album_id));

  if (error instanceof Error) {
    return <p className="alert alert-danger">{error?.message}</p>;
  }

  if (!album) {
    return <Loading />;
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <h1 className="display-2">{album.name}</h1>
          <p className="text-muted"> {album.id} </p>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album} />
        </div>
        <div className="col">
          <dl>
            <dt>Name</dt>
            <dd>{album.name}</dd>
            <dt>Artist</dt>
            <dd>{album.artists[0].name}</dd>
            <dt>Release date</dt>
            <dd>
              <LocalDate>{album.release_date}</LocalDate>
            </dd>
          </dl>

          <TracksList tracks={album.tracks?.items || []}/>
        </div>
      </div>
    </div>
  );
};

export const LocalDate = React.memo(({ children }: { children: string }) => (
  <>{new Date(children).toLocaleDateString()}</>
));

