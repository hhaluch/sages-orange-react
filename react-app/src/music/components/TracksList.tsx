import React, { useEffect, useRef, useState } from "react";
import { cls } from "../../core/helpers";
import { Track } from "../../core/model/Album";

type Props = {
  /**
   * List of tracks
   */
  tracks: Track[];
};

export const TracksList = ({ tracks }: Props) => {
  const [currentTrack, setCurrentTrack] = useState<Track>();
  const audioPlayerRef = useRef<HTMLAudioElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [volume, setVolume] = useState(0.2);

  useEffect(() => {
    if (!audioPlayerRef.current) return;
    audioPlayerRef.current.src = currentTrack?.preview_url || "";
    audioPlayerRef.current.volume = volume;
    isPlaying ? audioPlayerRef.current.play() : audioPlayerRef.current.pause();
  }, [currentTrack, isPlaying, volume]);

  const playTrack = (track: Track | undefined) => {
    if (currentTrack === track) {
      setIsPlaying(!isPlaying);
    } else {
      setIsPlaying(true);
    }
    setCurrentTrack(track);
  };

  if (!tracks || tracks.length === 0) {
    return (
      <div className="list-group">
        <div className="list-group-item text-center text-muted">No Tracks</div>
      </div>
    );
  }
  return (
    <div>
      <audio
        className="w-100 my-3"
        // src={currentTrack?.preview_url}
        controls
        ref={audioPlayerRef}
      />

      {tracks.map((track, index) => {
        return (
          <div
            key={track.id}
            onClick={() => playTrack(track)}
            className={cls(
              "list-group-item list-group-item-action",
              currentTrack?.id === track.id && "active"
            )}
          >
            {track.track_number}. {track.name}
            <span className="float-end">
              <TimeDuration time={track.duration_ms} />
            </span>
          </div>
        );
      })}
    </div>
  );
};

export const TimeDuration = React.memo(
  ({ time }: { time: string | number }) => {
    const d = new Date(time);
    return (
      <>
        {[
          ("0" + d.getUTCHours()).slice(-2),
          ("0" + d.getUTCMinutes()).slice(-2),
          ("0" + d.getUTCSeconds()).slice(-2),
        ].join(":")}
      </>
    );
  }
);
