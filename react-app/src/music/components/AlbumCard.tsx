import React from "react";
import { Link } from "react-router-dom";
import { Album } from "../../core/model/Album";

type Props = {
  // onClick?: React.MouseEventHandler<HTMLDivElement> | undefined;
  album: Album;
} & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

export const AlbumCard = ({ album, ...restProps }: Props) => {
  return (
    // <Link to={"/search?id="+album.id}></Link>
    <div className="card" {...restProps}>
      <img src={album.images[0].url} className="card-img-top" />
      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
      </div>
    </div>
  );
};
