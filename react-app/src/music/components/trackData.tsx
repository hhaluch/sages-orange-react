import { Track } from "../../core/model/Album";

export const trackData: Track[] = [
    {
        disc_number: 1,
        duration_ms: 60000,
        id: "6Uj1ctrBOjOas8xZXGqKk4",
        name: "Woman 1",
        preview_url: "https://p.scdn.co/mp3-preview/5f583b2ce5aff7762b6611a85907d55349a62db1?cid=ca8145259c0a468589cb3842f23014a0",
        track_number: 1,
        type: "track",
    },
    {
        disc_number: 1,
        duration_ms: 90000,
        id: "5Tby0U5VndHW0SomYO7Id7",
        name: "Woman 2",
        preview_url: "https://p.scdn.co/mp3-preview/5f583b2ce5aff7762b6611a85907d55349a62db1?cid=ca8145259c0a468589cb3842f23014a0",
        track_number: 2,
        type: "track",
    },
    {
        disc_number: 1,
        duration_ms: 3690000,
        id: "5j6gsQqT3UZeSghw0Xf3Pi",
        name: "Very long track name 3",
        preview_url: "https://p.scdn.co/mp3-preview/5f583b2ce5aff7762b6611a85907d55349a62db1?cid=ca8145259c0a468589cb3842f23014a0",
        track_number: 3,
        type: "track",
    },
];
