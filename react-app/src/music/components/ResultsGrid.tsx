import React from "react";
import { useNavigate } from "react-router-dom";
import { Album } from "../../core/model/Album";
import { AlbumCard } from "./AlbumCard";

type Props = { results: Album[] };

export const ResultsGrid = ({ results }: Props) => {
  // const results = props.results
  // const { results } = props;
  const navigate = useNavigate();

  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 g-0">
        {results.map((result, index) => {
          return (
            <div className="col" key={result.id}>
              <AlbumCard
                album={result}
                onClick={() =>
                  navigate({
                    pathname: "/albums/"+ result.id,
                    // search: "?id=" + result.id,
                  })
                }
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};
