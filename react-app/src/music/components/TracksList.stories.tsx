// Button.stories.ts|tsx

import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";

import { TracksList } from "./TracksList";
import { trackData } from "./trackData";

export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "Music/TracksList",
  component: TracksList,
} as ComponentMeta<typeof TracksList>;

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof TracksList> = (args) => (
  <TracksList {...args} />
);

//👇 Each story then reuses that template
export const NoTracks = Template.bind({});

NoTracks.args = {
  tracks: [],
};

export const WithTracks = Template.bind({});
WithTracks.args = {
  tracks: trackData,
};
