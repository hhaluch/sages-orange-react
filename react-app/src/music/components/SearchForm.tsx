import React, { useEffect, useRef, useState } from "react";

type Props = {
  value: string;
  onSearch: (query: string) => void;
};

export const SearchForm = ({ onSearch, value }: Props) => {
  const [query, setQuery] = useState(value);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(event.currentTarget.value);
  };

  const searchInputRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    // document.getElementById('search_input')?.focus()
    searchInputRef.current?.focus();
  }, []);

  // sync query with parent
  useEffect(() => {
    setQuery(value);
  }, [value]);

  useEffect(() => {
    if (query === value) return;

    const handler = setTimeout(() => {
      onSearch(query);
    }, 500);

    return () => {
      clearTimeout(handler);
    };
  }, [query, onSearch]);

  const sendSearch = () => {
    onSearch(query);
  };

  return (
    <div>
      <div className="input-group mb-3">
        <input
          // id="search_input"
          ref={searchInputRef}
          type="text"
          className="form-control"
          placeholder="Search Albums"
          /* 2 way data binding: */
          value={query}
          onChange={handleChange}
          // onChange={e => setQuery(e.currentTarget.value)}
        />
        <button
          className="btn btn-outline-secondary"
          type="button"
          onClick={sendSearch}
        >
          Search
        </button>
      </div>
    </div>
  );
};
