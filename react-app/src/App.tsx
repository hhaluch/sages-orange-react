import React, { Suspense, useEffect, useState } from "react";
import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.css";
import { PlaylistView } from "./playlists/containers/PlaylistView";
// import { AlbumSearchView } from "./music/containers/AlbumSearchView";
import { OAuthCallback } from "react-oauth2-hook";
import { useAuth } from "./useAuth";
import { Navigate, Route, Routes } from "react-router-dom";
import {} from "react-router";
import { NavBar } from "./core/components/NavBar";
import { AlbumDetailView } from "./music/containers/AlbumDetailView";
import { fetchCurrentUser } from "./core/services/SpotifyAPIService";
import { UserProfile } from "./core/model/User";
import { PlaylistsContainerView } from "./playlists/containers/PlaylistsContainerView";
import { SelectedPlaylistDetails } from "./playlists/containers/SelectedPlaylistDetails";
import { SelectedPlaylistEditor } from "./playlists/containers/SelectedPlaylistEditor";
import { Loading } from "./core/components/Loading";

const MusicSearch = React.lazy(
  () => import("./music/containers/AlbumSearchView")
);

function App() {
  // callback/#access_token
  // if (window.location.hash.includes("access_token")) {
  //   return <OAuthCallback />;
  // }

  // .container>.row>.col
  return (
    <>
      <NavBar />
      <div className="container mt-3">
        <div className="row">
          <div className="col">
            <Routes>
              <Route path="/callback" element={<OAuthCallback />} />
              <Route path="/" element={<Navigate to="/search" />} />
              <Route
                path="/search"
                element={
                  <Suspense fallback={<Loading />}>
                    <MusicSearch />
                  </Suspense>
                }
              />

              {/* <Route path="/playlists" element={<PlaylistView />} /> */}
              <Route path="/playlists/" element={<PlaylistsContainerView />}>
                <Route index element={<p>Please select playlist</p>} />
              </Route>

              <Route
                path="/playlists/:playlist_id"
                element={<PlaylistsContainerView />}
              >
                <Route index element={<SelectedPlaylistDetails />} />
                <Route path="edit" element={<SelectedPlaylistEditor />} />
              </Route>
              <Route path="/albums/:album_id" element={<AlbumDetailView />} />
              <Route
                path="*"
                element={
                  <h1 className="display-4 mt-5 text-center">
                    404 Page not found
                  </h1>
                }
              />
            </Routes>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
