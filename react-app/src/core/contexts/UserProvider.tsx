import React, { useEffect, useState } from "react";
import { useAuth } from "../../useAuth";
import { UserProfile } from "../model/User";
import { fetchCurrentUser } from "../services/SpotifyAPIService";

interface UserContext {
  user?: UserProfile;
  login: () => void;
  logout: () => void;
}

export const UserContext = React.createContext<UserContext>({} as UserContext);

type Props = {
  children: React.ReactNode;
};

export const UserProvider = ({ children }: Props) => {
  const { token, getToken, setToken } = useAuth();

  const [user, setUser] = useState<UserProfile>();

  useEffect(() => {
    if (!token) {
      setUser(undefined);
      return;
    }
    fetchCurrentUser().then(setUser);
  }, [token]);

  const login = () => {
    getToken();
  };
  const logout = () => {
    setToken(undefined);
  };

  const childContext = {
    user,
    login,
    logout,
  };

  return (
    <UserContext.Provider value={childContext}>{children}</UserContext.Provider>
  );
};
