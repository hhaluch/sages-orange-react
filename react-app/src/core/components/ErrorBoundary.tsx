import React, { ErrorInfo } from "react";

export class ErrorBoundary extends React.Component {
  state = { hasError: false };
  
  constructor(props: any) {
    super(props);
  }

  static getDerivedStateFromError(error: unknown) {
    // Zaktualizuj stan, aby następny render pokazał zastępcze UI.
    return { hasError: true };
  }

  override componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // Możesz także zalogować błąd do zewnętrznego serwisu raportowania błędów
    // logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // Możesz wyrenderować dowolny interfejs zastępczy.
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}
