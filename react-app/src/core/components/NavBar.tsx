import React, { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { cls } from "../helpers";
import { UserWidget } from "./UserWidget";

type Props = {};

export const NavBar = (props: Props) => {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <div className="container">
          <NavLink
            className={({ isActive }) =>
              `navbar-brand ${isActive ? "active" : ""}`
            }
            to="/"
          >
            MusicApp
          </NavLink>
          <button
            className="navbar-toggler"
            type="button"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
            onClick={() => setOpen(!open)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className={cls("collapse navbar-collapse", open && "show")}
            // className={"collapse navbar-collapse" + (open ? " show" : "")}
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/search">
                  Search
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="ml-auto navbar-text">
            <UserWidget />
          </div>
        </div>
      </nav>
    </div>
  );
};


