import React, { useContext } from "react";
import { UserContext } from "../contexts/UserProvider";

export const UserWidget = () => {
  const { user, login, logout } = useContext(UserContext);

  if (!user)
    return (
      <div>
        Welcome Guest |<span onClick={login}> Login</span>
      </div>
    );

  return (
    <div>
      Welcome {user.display_name} |<span onClick={logout}> Logout</span>
    </div>
  );
};
