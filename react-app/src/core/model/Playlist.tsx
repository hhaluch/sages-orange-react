export interface Playlist {
  id: string;
  name: string;
  public: boolean;
  description: string;
}

export function assertPlaylistsResponse(data: any): asserts data is Playlist[] {
  if (!Array.isArray(data)) {
    throw new Error("Invalid Server response");
  }
}
