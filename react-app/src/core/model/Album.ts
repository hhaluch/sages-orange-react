
export interface Artist {
    id: string;
    name: string;
    type: 'artist';
}

// Generated by https://quicktype.io

export interface Album {
    artists: Artist[];
    id:     string;
    images: Image[];
    name:   string;
    type:   'album';
    release_date: string,
    release_date_precision: "day"|'month',
    total_tracks: number,
    tracks?:PagingObject<Track>
}

export interface Track {
    id: string;
    name: string;
    type: "track";
    duration_ms: number;
    preview_url: string;
    track_number: number;
    disc_number: number;
}


export interface Image {
    url:    string;
    height: number;
    width:  number;
}

// Generated by https://quicktype.io

export interface AlbumSearchResponse{
    albums:PagingObject<Album>
}

export function assertAlbumSearchResponse(data:any): asserts data is AlbumSearchResponse{
    if(!(
        'albums' in data 
        && 'items' in data.albums 
        && Array.isArray(data.albums.items)
    )){
        throw new Error('Unexpected server response')
    }
}

export interface PagingObject<T> {
    href:     string;
    items:    T[];
    limit:    number;
    next:     string;
    offset:   number;
    previous: string;
    total:    number;
}


export function isSpotifyErrorResponse(data: any): data is SpotifyErrorResponse {
    return data && "error" in data && "message" in data.error;
  }
  interface SpotifyErrorResponse {
    error: { message: string; status: number };
  }
  