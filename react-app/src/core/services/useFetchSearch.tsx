import { useState, useEffect } from "react";

export function useFetchSearch<T, P>(
  currentQuery: P | undefined,
  fetcherFn: (query?: P) => Promise<T>
) {
  const [results, setResults] = useState<T | undefined>();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | undefined>();

  useEffect(() => {
    if (!currentQuery) return;

    setError(undefined);
    setIsLoading(true);
    fetcherFn(currentQuery)
      .then((albums) => setResults(albums))
      .catch((error) => setError(error?.message))
      .finally(() => setIsLoading(false));
  }, [currentQuery]);

  return { data: results, loading: isLoading, error };
}
