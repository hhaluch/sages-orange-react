import { Album } from "../model/Album";

export const albumData: Album[] = [
  {
    id: "123",
    name: "Test 123",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/300/300" },
    ],
  },
  {
    id: "234",
    name: "Test 234",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/200/200" },
    ],
  },
  {
    id: "345",
    name: "Test 345",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/400/400" },
    ],
  },
  {
    id: "456",
    name: "Test 456",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/250/250" },
    ],
  },
] as Album[]
