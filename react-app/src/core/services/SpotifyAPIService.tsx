import axios, { AxiosError } from "axios";
import { playlistsData } from "../../playlists/containers/playlistsData";
import { Album, assertAlbumSearchResponse, PagingObject } from "../model/Album";
import { assertPlaylistsResponse, Playlist } from "../model/Playlist";
import { UserProfile } from "../model/User";

export async function fetchCurrentUser() {
  const { data } = await axios.get<unknown>("me");
  assertsUserProfile(data);
  return data;
}

// https://github.com/colinhacks/zod#parse
function assertsUserProfile(data: any): asserts data is UserProfile {
  if (!("type" in data && "user" === data.type))
    throw new Error("Invalid server response");
}

// import { albumData } from "./albumData";
// export function fetchAlbumSearchResults(query = "") {
//   return new Promise<Album[]>((resolve) => {
//     setTimeout(() => {
//       resolve(albumData);
//     }, 1500);
//   });
// }

/**
 *
 * @param query
 * @param options
 * @returns
 */
export async function fetchAlbumSearchResults(
  query = "",
  options?: { limit: number; offset: number }
): Promise<Album[]> {
  const { data } = await axios.get<unknown>(
    "search",
    {
      params: {
        type: "album",
        query,
        ...options,
      },
    }
  );
  assertAlbumSearchResponse(data);
  return data.albums.items;
}

/**
 *
 * @returns
 */
export const fetchPlaylists = async (): Promise<Playlist[]> => {
  // return new Promise((resolve) =>
  //   setTimeout(() => {
  //     resolve(playlistsData);
  //   }, 1000)
  // );

  const { data } = await axios.get<PagingObject<Playlist>>(
    "me/playlists/"
  );
  return data.items;
};

/**
 *
 * @param id
 * @returns
 */

export async function fetchPlaylistById(
  id?: Playlist["id"]
): Promise<Playlist> {
  if (!id) throw new Error("Missing  Id");

  const { data } = await axios.get<Playlist>(
    "playlists/" + id
  );
  // return new Promise((resolve) =>
  //   setTimeout(() => {
  //     resolve(playlistsData[1]);
  //   }, 1000)
  // );
  return data;
}

export async function updatePlaylist(draft: Playlist) {
  // playlistsData[1] = draft;
  const { data } = await axios.put<Playlist>(
    "playlists/" + draft.id,
    {
      name: draft.name,
      public: draft.public,
      description: draft.description,
    }
  );
  return draft;
}

/**
 *
 * @param id
 * @returns
 */
export const fetchAlbumById = async (id?: Album["id"]): Promise<Album> => {
  if (!id) throw new Error("Missing Album Id");

  const { data } = await axios.get<unknown>(
    "albums/" + id
  );
  assertAlbumResponse(data);
  return data;
};

export function assertAlbumResponse(data: any): asserts data is Album {
  if (!(data.type === "album")) throw new Error("Invalid server response");
}
