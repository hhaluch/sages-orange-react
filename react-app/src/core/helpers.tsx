const helpers = {};
// https://www.npmjs.com/package/classnames

export const cls = (...classes: (string | boolean)[]): string => {
  return classes.filter((c) => typeof c === "string").join(" ");
};
