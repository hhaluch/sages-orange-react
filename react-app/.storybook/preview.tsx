import { QueryClient, QueryClientProvider } from "react-query";
import { MemoryRouter as Router } from "react-router-dom";
import { rest } from "msw";
import { initialize, mswDecorator } from "msw-storybook-addon";
import { UserContext } from "../src/core/contexts/UserProvider";
import { UserProfile } from "../src/core/model/User";
// Initialize MSW
initialize();

import "bootstrap/dist/css/bootstrap.css";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  msw: {
    handlers: [
      rest.get("/me", (req, res, ctx) => {
        return res(
          ctx.json({
            display_name: "MockUser",
            id: "123",
          })
        );
      }),
    ],
  },
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnMount: false,
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retry: false,
      retryOnMount: false,
    },
  },
});

const MockUserProvider = ({ children }) => {
  return (
    <UserContext.Provider
      value={
        {
          user: {
            id: "123",
            display_name: "Mock User",
          } as UserProfile,
        } as any
      }
    >
      {children}
    </UserContext.Provider>
  );
};

export const decorators = [
  // Provide the MSW addon decorator globally
  mswDecorator,
  (Story) => (
    <QueryClientProvider client={queryClient}>
      <MockUserProvider>
        <Router>
          <Story />
        </Router>
      </MockUserProvider>
    </QueryClientProvider>
  ),
];
